﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RidingCoin : MonoBehaviour {


    float cosDelta;
    float zeroX;
    float posDelta = 2f;
    public float rollingSpeed = 3;
    void Start () {
        cosDelta = Random.Range(0, 360);
        zeroX = transform.localPosition.x;
	}

    // Update is called once per frame
    Vector3 tempPos;
	void Update () {
        tempPos = transform.localPosition;
        tempPos.x = zeroX + Mathf.Sin(cosDelta) * posDelta;
        cosDelta += Time.deltaTime*rollingSpeed;
        transform.localPosition = tempPos;
	}
}

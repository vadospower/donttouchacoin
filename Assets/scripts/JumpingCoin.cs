﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingCoin : MonoBehaviour {

    Transform tr;
    Vector3 zeroPos;
    float sinDelta;
    float posDelta = 1f;
    Vector3 tempPos;

    // Use this for initialization
    void Start()
    {
        tr = transform;
        zeroPos = tr.localPosition;
        tempPos = tr.localPosition;

        sinDelta = Random.Range(0, 360);
    }

    // Update is called once per frame
    void Update()
    {
        sinDelta += Time.deltaTime;
        tempPos.y = zeroPos.y + posDelta * Mathf.Sin(sinDelta);
        tr.localPosition = tempPos;
    }
}

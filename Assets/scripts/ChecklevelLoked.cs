﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChecklevelLoked : MonoBehaviour {


    public int thisButtonlevel;
    public GameObject LevelLockedLayer;
    public GameObject GOwithButton;
	// Use this for initialization
	void Start () {

        if (PlayerPrefs.GetInt("levelprogress") >= thisButtonlevel)
        {
            GOwithButton.GetComponent<Button>().enabled = true;
            LevelLockedLayer.SetActive(false);
        }
        else
        {
            GOwithButton.GetComponent<Button>().enabled = false;
            LevelLockedLayer.SetActive(true); 


        }


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

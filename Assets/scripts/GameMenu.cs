﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenu : MonoBehaviour {


    public static GameMenu instance;
    public GameObject GameMenuPanel;
    public GameObject LoadingBackGroundPanel;
    public GameObject GameMenuButton;
    public GameObject LossPanel;
    public GameObject WinPanel;

    public string[] levels;
	// Use this for initialization
	void Start () {
        if (instance == null) instance = this;

    }
    public void OffAllPanel()
    {
        Time.timeScale = 0f;
        GameMenuButton.SetActive(false);
        GameMenuPanel.SetActive(false);
        LossPanel.SetActive(false);
        WinPanel.SetActive(false);
    }
    public void GameMenuEnabled()
    {
        OffAllPanel();
        GameMenuPanel.SetActive(true);
    }
    public void LossPanelEnabled()
    {
        OffAllPanel();
        LossPanel.SetActive(true);

    }
    public void WinPanelEnabled()
    {
        OffAllPanel();
        WinPanel.SetActive(true);
    }

    public void ReSpawnLevel()
    {
        ReSpawnlevel.RespawnLevelStatic();
        OffAllPanel();
        GameMenuButton.SetActive(true);
        Time.timeScale = 1f;
    }

    public void ToMainMenu()
    {
        LoadingBackGroundPanel.SetActive(true);
        UnityEngine.SceneManagement.SceneManager.LoadScene("menu");

    }
    
    public void ResumeGame()
    {
        OffAllPanel();
        GameMenuButton.SetActive(true);
        Time.timeScale = 1f;
    }
    public void NextLevel()
    {
        if (PlayerPrefs.GetInt("levelprogress") <= 5)
        {
          
            LoadingBackGroundPanel.SetActive(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene(levels[PlayerPrefs.GetInt("levelprogress")]);
        }
        else
        {
            LoadingBackGroundPanel.SetActive(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}

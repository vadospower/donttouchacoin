﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    public string[] levels;
    public GameObject MainMenuCanvas;
    public GameObject LevelsChooseCanvas;
    public GameObject loadingScreen;

    public bool DeletePlayerPrtefs;
	// Use this for initialization
	void Start () {

        if (DeletePlayerPrtefs)
            PlayerPrefs.DeleteAll();



        Time.timeScale = 1f;

        if (!PlayerPrefs.HasKey("levelprogress"))
        {
            PlayerPrefs.SetInt("levelprogress",1);
        }
        print(PlayerPrefs.GetInt("levelprogress"));
    }
    public void PlayButton()
    {
        loadingScreen.SetActive(true);
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(levels[PlayerPrefs.GetInt("levelprogress")]);
    }
    public void ClickLevelButton()
    {
        MainMenuCanvas.SetActive(false);
        LevelsChooseCanvas.SetActive(true);
    }
    public void BackToMainMenu()
    {
        MainMenuCanvas.SetActive(true);
        LevelsChooseCanvas.SetActive(false);
    }
    public void ClickOnLevel(int levelNumber)
    {
        print("click on level");
        loadingScreen.SetActive(true);
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(levels[levelNumber]);
    }


	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCoins : MonoBehaviour {


    public Transform parentTr;
    Vector3 parentUp;
	// Use this for initialization
	void Start () {
        parentUp = parentTr.up;


    }

    // Update is called once per frame
    void Update () {
      ///  transform.RotateAroundLocal(parentUp, Time.deltaTime * 10f);

        transform.RotateAround(parentTr.position, parentUp, angle: Time.deltaTime * 140f);

     /////transform.Rotate(parentUp, Time.deltaTime * 10f);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinKillerScript : MonoBehaviour {

    public float[] roadsPos;
    public bool Killthis = true;

    private void Awake()
    {
       
    }
    // Use this for initialization
    void Start () {
        if (roadsPos.Length != 0)
        {
            Vector3 newPos = transform.position;
            newPos.x = roadsPos[Random.Range(0, 3)];
            transform.position = newPos;
        }
        StartCoroutine(DestroyThisAfterSecond());
	}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("coin"))
        {
            Destroy(coll.gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
    IEnumerator DestroyThisAfterSecond()
    {
            yield return new WaitForSeconds(0.9f);
        if(Killthis) Destroy(gameObject);        
    }


}

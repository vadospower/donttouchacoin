﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleCoin : MonoBehaviour {

    Vector3 target;
    public Vector3[] points;
    int currentPoint;

	// Use this for initialization
	void Start () {
        currentPoint = Random.Range(0, points.Length);
        target = points[currentPoint];
	}
	
	// Update is called once per frame
	void Update () {

        if(Vector3.SqrMagnitude(transform.localPosition - points[currentPoint])<0.1 )
        {
            if (currentPoint < points.Length-1)
            {
                currentPoint++;
                target = points[currentPoint];
            }
            else
            {
                currentPoint = 0;
                target = points[currentPoint];
            }
        }


        transform.localPosition = Vector3.Lerp(transform.localPosition, target, Time.deltaTime*2f);


		
	}
}

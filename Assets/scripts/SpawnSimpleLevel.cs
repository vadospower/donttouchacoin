﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSimpleLevel : MonoBehaviour {

    public float sectorOffset;
    public int targetSumSectors;
    int currentSumSectors;
    public int sumSectorsOnStart;
    public float sectorYangle;

    float NextSpawnDistance;
    float OldHeroRunDistance;
    float HeroRunnedDistance = 0;
    Transform HeroTr;

    public List<GameObject> sectorList;

    List<GameObject> instantiateSectorlist;
    Vector3 countNextPos;
    Quaternion sectorRot;

    private void OnEnable()
    {
        ReSpawnlevel.respawnlevelevent += RestartLevel;
    }
    private void OnDisable()
    {
        ReSpawnlevel.respawnlevelevent -= RestartLevel;
    }
    void Start ()
    {
       

        HeroTr = PlayerController.Instance.transform;
        SpawnLevel();

	}
    public void SpawnLevel()
    {
        countNextPos = new Vector3(0,0,0);
        sectorRot = Quaternion.Euler(0, sectorYangle, 0);

        instantiateSectorlist = new List<GameObject>();
        for (int i = 0; i < sumSectorsOnStart; i++)
        {
            tempSector = Instantiate(sectorList[Random.Range(0, sectorList.Count)], countNextPos, sectorRot);
            instantiateSectorlist.Add(tempSector);
            countNextPos.z += sectorOffset;
            currentSumSectors++;
        }

        NextSpawnDistance = sectorOffset;
        OldHeroRunDistance = 0f;
    }

    GameObject tempSector;
	void Update () {

        CheckNeedMoveSector();
        CheckEndLevel();
    }

    void CheckEndLevel()
    {
        if (currentSumSectors > targetSumSectors )
        {
            LevelInfoSave.Instance.UpdateProgress();
            GameMenu.instance.WinPanelEnabled();
        }
    }

    void CheckNeedMoveSector()
    {
        HeroRunnedDistance += HeroTr.position.z - OldHeroRunDistance;
        OldHeroRunDistance = HeroTr.position.z;
       
        if (currentSumSectors <= targetSumSectors && HeroRunnedDistance > NextSpawnDistance)
        {
            instantiateSectorlist[0].transform.position = countNextPos;
            instantiateSectorlist[0].transform.rotation = sectorRot;

            tempSector = instantiateSectorlist[0];
            instantiateSectorlist.RemoveAt(0);
            instantiateSectorlist.Insert(instantiateSectorlist.Count , tempSector);

            countNextPos.z += sectorOffset;
            NextSpawnDistance += sectorOffset;
            currentSumSectors++;
        }

    }
    public void RestartLevel()
    {
        HeroRunnedDistance = 0;
        currentSumSectors = 0;
        countNextPos = new Vector3(0, 0, 0);
        sectorRot = Quaternion.Euler(0, sectorYangle, 0);

        for (int i = 0; i < instantiateSectorlist.Count; i++)
        {
            instantiateSectorlist[i].transform.position = countNextPos;
            instantiateSectorlist[i].transform.rotation = sectorRot;
            
            countNextPos.z += sectorOffset;
            currentSumSectors++;
        }

        NextSpawnDistance = sectorOffset;
        OldHeroRunDistance = 0;

    }


}

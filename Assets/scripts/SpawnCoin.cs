﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoin : MonoBehaviour {

    public GameObject CoinPrefab;
    Quaternion spawnQuat ;
    Vector3 zeroPoint;
    public float HeightCoins;
    public int numberOfRows;
    public float widthOffset;
    public float lengthOffset;
    Transform parentTr;

    // Use this for initialization
    float lengthCounter;
    float widthCounter; 
    void Start () {
        zeroPoint = transform.position;
        parentTr = transform;
        spawnQuat = new Quaternion();
        Vector3 startRot  =new Vector3 (-90, 0, 0);
        spawnQuat.eulerAngles = startRot;
        
        

        lengthCounter = zeroPoint.z;
        widthCounter = zeroPoint.x;
        for (int i = 0; i < numberOfRows; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                GameObject tempGo = Instantiate(CoinPrefab, new Vector3(widthCounter, HeightCoins, lengthCounter), spawnQuat);
                tempGo.transform.parent = parentTr;
                
                widthCounter += widthOffset;
            }
            lengthCounter += lengthOffset;
            widthCounter = zeroPoint.x;
            
        }


	}
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.3f);
    }
    public void SetZpos(float z)
    {
        Vector3 tempPos = transform.position;
        tempPos.z = z;
        transform.position = tempPos;
    }







    // Update is called once per frame
    void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Assets.Pixelation.Scripts;

public class PlayerController : MonoBehaviour
{
    public Pixelation postproc;
    float deathScreenFade=110;

    Animator anim;
    static public GameObject Instance;

    int currentRoad = 2;
    public float[] roadsPos;

    float SwipeLenght = Screen.height * 0.1f;

    float StartHeroHeight;
    bool OffOnAnimErrorCorrect;

    public delegate void SimpleDel();

    public static event SimpleDel OffAllParmsEvent;
    public static void StaticOffParams()
    {
        OffAllParmsEvent();
    }
    void OffAllAnimParams()
    {
        OffOnAnimErrorCorrect = true;
        anim.SetBool("slide", false);
        anim.SetBool("jump", false);
    }
    private void OnEnable()
    {
        OffAllParmsEvent += OffAllAnimParams;
        ReSpawnlevel.respawnlevelevent += ReSpawnIzzy;
    }
    private void OnDisable()
    {
        OffAllParmsEvent -= OffAllAnimParams;
        ReSpawnlevel.respawnlevelevent -= ReSpawnIzzy;
    }
    // Use this for initialization
    private void Awake()
    {
        if (Instance == null) Instance = gameObject;

    }
    void Start() {

        Time.timeScale = 1f;
        anim = GetComponent<Animator>();
        postproc.BlockCount = deathScreenFade;
        if (Instance == null) Instance = gameObject;

        StartHeroHeight = transform.position.y;
    }

    // Update is called once per frame
    void Update() {
        KeyBoardControl();
        if (Input.touchCount > 0)
            SwipeDetect();
        MoveToTargetRoad();
        if (OffOnAnimErrorCorrect) CorrectAnimError();

        CheckDeath();
        PostProccesFade();


    }

    void CheckDeath()
    {
        if (deathScreenFade < 8)
            GameMenu.instance.LossPanelEnabled();
    }

    void PostProccesFade()
    {

        if (deathScreenFade < 100)
        {
            postproc.enabled = true;
            if (deathScreenFade < 5) deathScreenFade = 10;
            deathScreenFade += Time.deltaTime*30f;
            postproc.BlockCount = deathScreenFade;
        }

        else postproc.enabled = false;


    }

    void CorrectAnimError()
    {
       
            Vector3 tempPos;
            tempPos = transform.position;
            tempPos.y = Mathf.Lerp(tempPos.y, StartHeroHeight, Time.deltaTime * 3f);
            transform.position = tempPos;
        
    }
    float turnLeft = -20;
    float turnRight= 20;
    float CurrentAngle = 0;
    void MoveToTargetRoad()
    {

        Vector3 tempPos;
        if (transform.position.x > roadsPos[currentRoad])
        {
            if (transform.position.x - roadsPos[currentRoad] < 0.6 )
            {
                CurrentAngle = Mathf.Lerp(CurrentAngle, 0, Time.deltaTime * 4f);
                transform.rotation = Quaternion.AngleAxis(CurrentAngle, Vector3.up);
                tempPos = transform.position;
                tempPos.x = Mathf.Lerp(tempPos.x,roadsPos[currentRoad],Time.deltaTime);
                transform.position = tempPos;
                return;
            }

            CurrentAngle = Mathf.Lerp(CurrentAngle, turnLeft, Time.deltaTime * 4f);
            transform.rotation = Quaternion.AngleAxis(CurrentAngle, Vector3.up);
        }
        else
        {
            if (roadsPos[currentRoad]- transform.position.x  < 0.6  )
            {
                CurrentAngle = Mathf.Lerp(CurrentAngle, 0, Time.deltaTime * 4f);
                transform.rotation = Quaternion.AngleAxis(CurrentAngle, Vector3.up);
                tempPos = transform.position;
                tempPos.x = Mathf.Lerp(tempPos.x, roadsPos[currentRoad], Time.deltaTime);
                transform.position = tempPos;
                return;
            }

            CurrentAngle = Mathf.Lerp(CurrentAngle, turnRight, Time.deltaTime * 4f);
            transform.rotation = Quaternion.AngleAxis(CurrentAngle, Vector3.up);
        }
    }

    void KeyBoardControl()
    {
        if (Input.GetKeyDown(KeyCode.A) && currentRoad > 1)
        {
            currentRoad--;
        }
        if (Input.GetKeyDown(KeyCode.D) && currentRoad < 3)
        {
            currentRoad++;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            anim.SetBool("slide", true);
            OffOnAnimErrorCorrect = false;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {

            anim.SetBool("jump", true);
            OffOnAnimErrorCorrect = false;
        }
    }

    Vector2 endPos;
    Vector2 beginPos;
    void SwipeDetect()
    {
        if (Input.touches[0].phase == TouchPhase.Began) beginPos = Input.touches[0].position;
        if (Input.touches[0].phase == TouchPhase.Ended) endPos = Input.touches[0].position;
           else return;

        if(Mathf.Abs(beginPos.x-endPos.x)>SwipeLenght || Mathf.Abs(beginPos.y - endPos.y) > SwipeLenght)
        {
            if (Mathf.Abs(beginPos.x - endPos.x) > Mathf.Abs(beginPos.y - endPos.y))
            {
                if (beginPos.x > endPos.x)
                {
                    if (currentRoad > 1)
                        currentRoad--;
                }
                else
                {
                    if (currentRoad < 3)
                        currentRoad++;
                }

            }
            else
            {
                if
                    (beginPos.y > endPos.y)
                {
                    anim.SetBool("slide", true);
                    OffOnAnimErrorCorrect = false;
                }
                else
                {
                    anim.SetBool("jump", true);
                    OffOnAnimErrorCorrect = false;
                }


            }


        }
    }
    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("coin")|| coll.CompareTag("movingcoin"))
        {
            deathScreenFade -= 80f;
            coll.gameObject.SetActive(false);
           
        }
    }

    void ReSpawnIzzy()
    {
        deathScreenFade = 70;
        transform.position = new Vector3(0, 0, 0);
        transform.rotation = new Quaternion(0, 0, 0, 0);
   
    }



}

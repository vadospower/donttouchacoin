﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotate : MonoBehaviour {

    Transform tr;
    Vector3 zeroPos;
    float sinDelta;
    float posDelta = 0.2f;
    Vector3 tempPos;

	// Use this for initialization
	void Start () {
        tr = transform;
        zeroPos = tr.localPosition;
        tempPos = tr.localPosition;
        tr.Rotate(0, 0, Random.Range(0f,360f));

        sinDelta = Random.Range(0, 1);
	}

    // Update is called once per frame
	void Update () {
        sinDelta += Time.deltaTime*6f;
        tempPos.y = zeroPos.y + posDelta * Mathf.Sin(sinDelta);
        tr.localPosition = tempPos;
        tr.Rotate(0,0, Time.deltaTime*60f);
        
	}
}

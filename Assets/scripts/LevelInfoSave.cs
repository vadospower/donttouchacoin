﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfoSave : MonoBehaviour {


    public int thisLevelNum;
    public static LevelInfoSave Instance;
	// Use this for initialization
	void Start () {
        if (Instance == null)
               Instance = this;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateProgress()
    {
        if (PlayerPrefs.GetInt("levelprogress") <= thisLevelNum && PlayerPrefs.GetInt("levelprogress") < 5)
        {
            PlayerPrefs.SetInt("levelprogress", PlayerPrefs.GetInt("levelprogress") + 1);
            print("level progress " + PlayerPrefs.GetInt("levelprogress"));
        }

    }

}
